# cloudstack-k8s-upgrader
cloudstack-k8s-upgrader upgrades the managed CSC Kubernetes cluster in
CloudStack to the next minor version whenever a new Kubernetes ISO is
available.

## Running the program
Make sure the following environment variables are set:
```bash
# these values must be obtained from the CloudStack web UI
export CLOUDSTACK_API_KEY=secret
export CLOUDSTACK_SECRET_KEY=secret
# name of Kubernetes cluster
export CLUSTER_NAME=Kube1
# notification emails will be sent here
export EMAIL_RECIPIENT=root@csclub.uwaterloo.ca
# path of SSH key used to SSH into VMs created in CloudStack
export SSH_KEY_PATH=~/.ssh/id_rsa_cloudstack
```
