package main

import (
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"git.csclub.uwaterloo.ca/cloud/cloudstack-k8s-upgrader/pkg/config"
	"git.csclub.uwaterloo.ca/cloud/cloudstack-k8s-upgrader/pkg/upgrader"
)

func setupLogging() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
}

func main() {
	setupLogging()
	cfg := config.New()
	k8sUpgrader := upgrader.New(cfg)
	k8sUpgrader.DoUpgrade()
}
