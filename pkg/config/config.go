package config

import "os"

// A Config holds all of the configuration values needed for the program.
type Config struct {
	CloudstackApiKey     string
	CloudstackSecretKey  string
	CloudstackApiBaseUrl string
	ClusterName          string
	SSHKeyPath           string
	EmailServer          string
	EmailSender          string
	EmailSenderName      string
	EmailRecipient       string
	EmailReplyTo         string
}

func New() *Config {
	cfg := &Config{
		CloudstackApiKey:    os.Getenv("CLOUDSTACK_API_KEY"),
		CloudstackSecretKey: os.Getenv("CLOUDSTACK_SECRET_KEY"),
		ClusterName:         os.Getenv("CLUSTER_NAME"),
		SSHKeyPath:          os.Getenv("SSH_KEY_PATH"),
		EmailRecipient:      os.Getenv("EMAIL_RECIPIENT"),
	}
	if cfg.CloudstackApiKey == "" {
		panic("CLOUDSTACK_API_KEY is empty or not set")
	}
	if cfg.CloudstackSecretKey == "" {
		panic("CLOUDSTACK_SECRET_KEY is empty or not set")
	}
	if cfg.ClusterName == "" {
		panic("CLUSTER_NAME is empty or not set")
	}
	if cfg.SSHKeyPath == "" {
		panic("SSH_KEY_PATH is empty or not set")
	}
	if cfg.EmailRecipient == "" {
		panic("EMAIL_RECIPIENT is empty or not set")
	}
	// These should never change
	cfg.CloudstackApiBaseUrl = "https://cloud.csclub.uwaterloo.ca/client/api"
	cfg.EmailServer = "mail.csclub.uwaterloo.ca:25"
	cfg.EmailSender = "cloudstack-k8s-upgrader@csclub.uwaterloo.ca"
	cfg.EmailSenderName = "cloudstack-k8s-upgrader"
	cfg.EmailReplyTo = "no-reply@csclub.uwaterloo.ca"
	return cfg
}
