module git.csclub.uwaterloo.ca/cloud/cloudstack-k8s-upgrader

go 1.19

require (
	github.com/rs/zerolog v1.28.0
	golang.org/x/net v0.5.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.4.0 // indirect
)
